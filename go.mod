module modernc.org/bitz

go 1.21

require (
	github.com/dgryski/go-bitstream v0.0.0-20180413035011-3522498ce2c8
	github.com/dustin/go-humanize v1.0.1
	github.com/icza/huffman v0.0.0-20230330133829-d543610fbdd2
)
