# bitz

Package bitz is a simple, limited and sub-optimal variable bit length
encoder/decoder for a set of symbols with known distribution.
