.PHONY:	clean edit editor

clean:
	rm -f log-* cpu.test mem.test *.out
	go clean

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile go.mod builder.json *.go & fi

editor:
	gofmt -l -s -w *.go
	go test -o /dev/null -c
	go install -v  2>&1 | tee log-editor
	staticcheck
	go test 2>&1 | tee log-editor
