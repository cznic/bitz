// Package bitz is a simple, limited and sub-optimal variable bit length
// encoder/decoder for a set of symbols with known distribution. The
// compression factor achieved is comparable to that of Huffman encoding.
package bitz

import (
	"fmt"
	"io"
	"math"
	"math/bits"
	"sort"
)

type SymbolFrequency struct {
	Symbol uint32
	// How many times 'Symbol' occurs in a message (a sequence of symbols).
	Frequency uint32
}

func (s SymbolFrequency) String() string {
	return fmt.Sprintf("{%#U %v(%0[2]b)}", s.Symbol, s.Frequency)
}

type encoding struct {
	// len  byte
	// bits uint32
	b [5]byte
}

func newEncoding(l int, bits uint32) (r encoding) {
	r.b[0] = byte(l)
	bits <<= 32 - l
	for i := 0; i < 4; i++ {
		r.b[4-i] = byte(bits)
		bits >>= 8
	}
	return r
}

func (e encoding) String() string {
	return fmt.Sprintf("%0b, %v (|% x|)", e.bits(), e.len(), e.b[:])
}

func (e *encoding) len() int {
	return int(e.b[0])
}

func (e *encoding) bits() (r uint32) {
	for i := 0; i < 4; i++ {
		r = r<<8 | uint32(e.b[i+1])
	}
	return r >> (32 - e.len())
}

type Codec struct {
	bitWidth  int
	encodings map[uint32]encoding // symbol: encoding
	root      *node
	run       uint64
}

// NewCodec returns a newly created Codec. The backing array of the 'items'
// argument is modified by NewCodec.
//
// Symbols with zero frequency are ignored.
//
// The minimum cardinality of the symbol set is 3 and the minimum sum of symbol
// frequencies is 3.
func NewCodec(items []SymbolFrequency) (r *Codec, err error) {
	return newCodec(items, true, true, true)
}

func newCodec(items []SymbolFrequency, spread, postfix, prune bool) (r *Codec, err error) {
again:
	w := 0
	var sum, run uint64
	for len(items) < 3 {
		items = append(items, SymbolFrequency{Symbol: math.MaxUint32 - uint32(len(items)), Frequency: 1})
	}
	for _, v := range items {
		if v.Frequency != 0 {
			items[w] = v
			sum += uint64(v.Frequency)
			w++
		}
	}
	if w < 2 {
		items = append(items, SymbolFrequency{Symbol: math.MaxUint32 - uint32(len(items)), Frequency: 2 - uint32(w)})
		sum = 0
		run = 0
		goto again
	}

	items = items[:w]

	sort.Slice(items, func(i, j int) (r bool) {
		a, b := items[i].Frequency, items[j].Frequency
		r = a < b || a == b && items[i].Symbol > items[j].Symbol
		return !r
	})

	if spread && bits.OnesCount64(sum) != 1 {
		w := 64 - bits.LeadingZeros64(sum-1)
		wantSum := uint64(1) << w
		diff := wantSum - sum
		diff0 := diff
		for diff != 0 {
			for i, v := range items {
				if diff == 0 {
					break
				}

				donate := uint64(v.Frequency) * diff0 / sum
				switch {
				case donate == 0:
					donate = 1
				case donate > diff:
					donate = diff
				}
				items[i].Frequency += uint32(donate)
				diff -= donate
			}
		}
	}

	for i, v := range items {
		run0 := run
		run += uint64(v.Frequency)
		if run < run0 {
			return nil, errorf("sum of frequencies exceeds max uint32")
		}

		v.Frequency = uint32(run0)
		items[i] = v
	}
	r = &Codec{
		bitWidth:  64 - bits.LeadingZeros64(run-1),
		encodings: map[uint32]encoding{},
		root:      &node{},
		run:       run,
	}
	if postfix {
		r.postfix(items)
	}
	for i, v := range items {
		if _, ok := r.encodings[v.Symbol]; ok {
			return nil, errorf("duplicate code: %#U", v.Symbol)
		}

		enc := r.newEncoding(items, i)
		r.encodings[v.Symbol] = enc
		r.register(v.Symbol, enc)
	}
	if prune {
		r.prune(r.root, 0, 0, false)
	}
	return r, nil
}

func (c *Codec) prune(n *node, path uint32, bits int, pruned bool) {
	for {
		switch {
		case n.Zero == nil && n.One == nil:
			// leaf
			if pruned {
				enc := newEncoding(bits, path)
				c.encodings[n.Symbol] = enc
			}
			return
		case n.Zero == nil && n.One != nil:
			*n = *n.One
			pruned = true
		case n.Zero != nil && n.One == nil:
			*n = *n.Zero
			pruned = true
		case n.Zero != nil && n.One != nil:
			c.prune(n.Zero, path<<1, bits+1, pruned)
			c.prune(n.One, (path<<1)|1, bits+1, pruned)
			return
		}
	}
}

func (c *Codec) postfix(items []SymbolFrequency) {
	for i := 0; i < len(items)-2; i++ {
		itemI := items[i]
		itemIp1 := items[i+1]
		var itemIp2 SymbolFrequency
		switch {
		case i+2 < len(items):
			itemIp2 = items[i+2]
		default:
			itemIp2.Frequency = uint32(c.run)
		}
		if itemIp2.Frequency-itemIp1.Frequency < 2 {
			// No room to manipulate item2
			break
		}

		mb := c.minBits(itemI.Frequency, itemIp1.Frequency)
		if mb == 1 {
			continue
		}

		mask := uint32(1)<<(c.bitWidth-mb+1) - 1
		run1 := itemIp1.Frequency&^mask + mask + 1
		if run1 >= itemIp2.Frequency {
			continue
		}

		mb12 := c.minBits(itemIp1.Frequency, itemIp2.Frequency)
		if c.minBits(run1, itemIp2.Frequency)-mb12 > 1 {
			continue
		}

		items[i+1].Frequency = run1
		itemIp1 = items[i+1]
	}
}

func (c *Codec) register(sym uint32, enc encoding) {
	n := c.root
	for m := uint32(1) << (enc.len() - 1); m != 0; m >>= 1 {
		switch {
		case enc.bits()&m != 0:
			if n.One == nil {
				n.One = &node{}
			}
			n = n.One
		default:
			if n.Zero == nil {
				n.Zero = &node{}
			}
			n = n.Zero
		}
	}
	n.Symbol = sym
}

func (c *Codec) newEncoding(items []SymbolFrequency, i int) (r encoding) {
	var l, l2 int
	bits := items[i].Frequency
	if i > 0 {
		prev := items[i-1].Frequency
		l = c.minBits(prev, bits)
	}
	if i < len(items)-1 {
		next := items[i+1].Frequency
		l2 = c.minBits(bits, next)
	}
	if l2 > l {
		l = l2
	}
	return newEncoding(l, bits>>(c.bitWidth-l))
}

// The minimum number of left bits, starting at s.bitWidth to differentiate 'a'
// from 'b'.
func (c *Codec) minBits(a, b uint32) (r int) {
	m := uint32(1) << (c.bitWidth - 1)
	x := a&m != 0
	y := b&m != 0
	r = 1
	for x == y && m != 0 {
		r++
		m >>= 1
		x = a&m != 0
		y = b&m != 0
	}
	return r
}

// Encoding returns the encoding of 'symbol'. Encoding is idempotent.
func (c *Codec) Encoding(symbol uint32) (nbits int, bits uint32) {
	enc, ok := c.encodings[symbol]
	if !ok {
		return -1, 0
	}

	return enc.len(), enc.bits()
}

type node struct {
	Zero   *node
	One    *node
	Symbol uint32
}

type BitReader interface {
	ReadBit() (bool, error)
}

// Decode attempts to decode the bitstream in r to a symbol.
func (c *Codec) Decode(r BitReader) (symbol uint32, bits int, err error) {
	n := c.root
	for {
		bit, err := r.ReadBit()
		if err != nil {
			if err == io.EOF {
				if bits != 0 {
					err = errorf("unexpected EOF")
				}
				return 0, bits, err
			}
		}

		bits++
		switch bit {
		case false:
			n = n.Zero
		default:
			n = n.One
		}
		if n == nil {
			return 0, bits, errorf("malformed input detected")
		}

		if n.Zero == nil && n.One == nil {
			return n.Symbol, bits, nil
		}
	}
}
