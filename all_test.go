package bitz

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"math/bits"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"text/tabwriter"

	"github.com/dgryski/go-bitstream"
	"github.com/icza/huffman"
)

const getty = `Fourscore and seven years ago our fathers brought forth on this
continent a new nation, conceived in liberty and dedicated to the
proposition that all men are created equal.
Now we are engaged in a great civil war, testing whether that nation
or any nation so conceived and so dedicated can long endure. We are
met on a great battle field of that war. We have come to dedicate a
portion of that field, as a final resting place for those who here
gave their lives that that nation might live. It is altogether
fitting and proper that we should do this.
But, in a larger sense, we can not dedicate - we can not consecrate
- we can not hallow - this ground. The brave men, living and dead,
who struggled here, have consecrated it, far above our poor power to
add or detract. The world will little note, nor long remember, what
we say here, but it can never forget what they did here. It is for
us the living, rather, to be dedicated here to the unfinished work
which they who fought here have thus far so nobly advanced. It is
rather for us to be here dedicated to the great task remaining
before us - that from these honored dead we take increased devotion
to that cause for which they gave the last full measure of devotion
- that we here highly resolve that these dead shall not have died in
vain - that this nation, under God, shall have a new birth of
freedom - and that government of the people, by the people, for the
people, shall not perish from the earth.
`

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

func Test(t *testing.T) {
	for _, test := range [][]SymbolFrequency{
		{},
		{
			{0, 1},
		},
		{
			{0, 1},
			{1, 1},
		},
		{
			{0, 2},
			{1, 1},
			{2, 1},
		},
		{
			{0, 6},
			{1, 1},
			{2, 1},
		},
		{
			{0, 3},
			{1, 2},
			{2, 1},
		},
		{
			{0, 4},
			{1, 3},
			{2, 2},
			{3, 1},
		},
		{
			{0, 5},
			{1, 4},
			{2, 3},
			{3, 2},
			{4, 1},
		},
		{
			{0, 6},
			{1, 5},
			{2, 4},
			{3, 3},
			{4, 2},
			{5, 1},
		},
		{
			{0, 4},
			{1, 2},
			{2, 1},
		},
		{
			{0, 8},
			{1, 4},
			{2, 2},
			{3, 1},
		},
		{
			{0, 16},
			{1, 8},
			{2, 4},
			{3, 2},
			{4, 1},
		},
		{
			{0, 32},
			{1, 16},
			{2, 8},
			{3, 4},
			{4, 2},
			{5, 1},
		},
		{
			{' ', 20},
			{'a', 40},
			{'m', 10},
			{'l', 7},
			{'f', 8},
			{'t', 15},
		},
	} {
		for _, spread := range []bool{false, true} {
			for _, postfix := range []bool{false, true} {
				for _, prune := range []bool{false, true} {
					test1 := append([]SymbolFrequency(nil), test...)
					bw := bitWidth(uint64(len(test1)))
					var bits0, bits1 int
					m := map[rune]uint32{}
					for _, v := range test {
						m[rune(v.Symbol)] = v.Frequency
						bits0 += bw * int(v.Frequency)
					}
					t.Logf("==== %v", test1)
					s, err := newCodec(test1, spread, postfix, prune)
					if err != nil {
						t.Fatal(err)
					}
					t.Log(test1)

					for _, v := range test {
						len, _ := s.Encoding(v.Symbol)
						bits1 += len * int(v.Frequency)
					}

					buf := bytes.NewBuffer(nil)
					tw := tabwriter.NewWriter(buf, 10, 4, 2, ' ', 0)

					for _, v := range test1 {
						nbits, bits := s.Encoding(v.Symbol)
						fmt.Fprintf(tw, "%#U:\t%0*b\t%2d\n", v.Symbol, nbits, bits, nbits)
					}
					tw.Flush()
					hbits := huffmanBits(m)
					t.Logf(".... spread=%v postfix=%v prune=%v bw=%v bits0=%s bits1=%s huffman bits=%s k=%4.2g\t%+d\n%s----",
						spread, postfix, prune, bw, h(bits0), h(bits1), h(hbits), float64(bits1)/float64(bits0), bits1-int(hbits),
						buf.Bytes())
				}
			}
		}
	}
}

func bitWidth(n uint64) int {
	return 64 - bits.LeadingZeros64(n-1)
}

func Test2(t *testing.T) {
	m := map[rune]uint32{}
	for _, r := range getty {
		m[r]++
	}
	var test []SymbolFrequency
	for k, v := range m {
		test = append(test, SymbolFrequency{Symbol: uint32(k), Frequency: v})
	}

	for _, spread := range []bool{false, true} {
		for _, postfix := range []bool{false, true} {
			for _, prune := range []bool{false, true} {
				test1 := append([]SymbolFrequency(nil), test...)
				bw := bitWidth(uint64(len(test1)))
				var bits0, bits1 int
				for _, v := range test {
					bits0 += bw * int(v.Frequency)
				}
				s, err := newCodec(test1, spread, postfix, prune)
				if err != nil {
					t.Fatal(err)
				}

				for _, v := range test {
					len, _ := s.Encoding(v.Symbol)
					bits1 += len * int(v.Frequency)
				}

				buf := bytes.NewBuffer(nil)
				tw := tabwriter.NewWriter(buf, 10, 4, 2, ' ', 0)

				for _, v := range test1 {
					nbits, bits := s.Encoding(v.Symbol)
					fmt.Fprintf(tw, "%#U:\t%0*b\t%2d\n", v.Symbol, nbits, bits, nbits)
				}
				tw.Flush()
				t.Logf("spread=%v postfix=%v prune=%v bw0=%v bw=%v bits0=%s bits1=%s k=%4.2g\n%s", spread, postfix, prune, bw, s.bitWidth, h(bits0), h(bits1), float64(bits1)/float64(bits0), buf.Bytes())
			}
		}
	}
}

func Test3(t *testing.T) {
	type testCase struct {
		nm, src string
	}
	tests := []testCase{
		{"abc", "abc"},
		{"getty", getty},
	}
	filepath.Walk(".", func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}

		if info.IsDir() {
			return nil
		}

		b, err := os.ReadFile(path)
		if err != nil {
			t.Fatal(err)
		}

		if len(b) != 0 {
			tests = append(tests, testCase{path, string(b)})
		}
		return nil
	})
	var sumK float64
	nk := 0
	for itest, test := range tests {
		g, k, _ := roundTrip(t, test.nm, test.src)
		sumK += k
		nk++
		if e := test.src; g != e {
			t.Fatalf("%v %s: got\n%q\nexp\n%q", itest, test.nm, g, e)
		}
	}
	t.Logf("avg k=%.4g", sumK/float64(nk))
}

type bitReader struct {
	r *bitstream.BitReader
}

func (b *bitReader) ReadBit() (bool, error) {
	bit, err := b.r.ReadBit()
	return bool(bit), err
}

func roundTrip(t *testing.T, nm, src string) (string, float64, int64) {
	m := map[rune]uint32{}
	for i := 0; i < len(src); i++ {
		r := rune(src[i])
		m[r]++
	}
	var test0 []SymbolFrequency
	for k, v := range m {
		test0 = append(test0, SymbolFrequency{Symbol: uint32(k), Frequency: v})
	}
	bw := bitWidth(uint64(len(test0)))
	var bits0, bits1 int
	for _, v := range test0 {
		bits0 += bw * int(v.Frequency)
	}
	//	spread	postfix	avg k	enwik8	enwik9
	//	false	false	0.9319	0.6959	0.6633
	//	false	true	0.9303	0.6954	0.6622
	//	true	false	0.8507	0.6405	0.6518
	//	true	true	0.8498	0.6403	0.6504
	c, err := newCodec(append([]SymbolFrequency(nil), test0...), true, true, true)
	if err != nil {
		t.Fatal(err)
	}

	for _, v := range test0 {
		len, _ := c.Encoding(v.Symbol)
		bits1 += len * int(v.Frequency)
	}

	buf := bytes.NewBuffer(nil)
	// tw := tabwriter.NewWriter(buf, 10, 4, 2, ' ', 0)

	// for _, v := range c.items {
	// 	nbits, bits := c.Encoding(v.Symbol)
	// 	fmt.Fprintf(tw, "%#U:\t%0*b\t%2d\n", v.Symbol, nbits, bits, nbits)
	// }
	// tw.Flush()
	k := float64(bits1) / float64(bits0)
	t.Logf("bw=%v bits0=%s(%s) bits1=%s(%s) k=%.4g\t%s\n%s", bw, h(bits0), h((bits0+7)/8), h(bits1), h((bits1+7)/8), k, nm, buf.Bytes())

	buf.Reset()
	w := bitstream.NewWriter(buf)
	wbits := 0
	for i := 0; i < len(src); i++ {
		r := rune(src[i])
		nbits, bits := c.Encoding(uint32(r))
		w.WriteBits(uint64(bits), nbits)
		wbits += nbits
	}
	w.Flush(bitstream.Zero)
	r := bitstream.NewReader(buf)
	rbits := 0
	var sb strings.Builder
	for rbits < wbits {
		sym, nbits, err := c.Decode(&bitReader{r})
		if err != nil {
			if err == io.EOF {
				break
			}

			t.Fatal(nm, err)
		}

		sb.WriteByte(byte(sym))
		rbits += nbits
	}
	if g, e := rbits, wbits; g != e {
		t.Fatalf("wbits=%v rbits=%v", wbits, rbits)
	}
	return sb.String(), k, int64(bits1)
}

func TestCompare(t *testing.T) {
	//			huffman	506,532,852	(63,316,607 bytes)
	//	spread	postfix	prune
	//	false	false	false	552,755,393	(69,094,425 bytes)
	//	false	false	true	521,198,021	(65,149,753 bytes)	*
	//	false	true	false	552,313,057	(69,039,133 bytes)
	//	false	true	true	520,763,724	(65,095,466 bytes)	*
	//	true	false	false	507,771,780	(63,471,473 bytes)
	//	true	false	true	507,771,590	(63,471,449 bytes)	*
	//	true	true	false	507,736,323	(63,467,041 bytes)
	//	true	true	true	507,736,323	(63,467,041 bytes)	=

	data, err := os.ReadFile(filepath.Join("testdata", "enwik8"))
	if err != nil {
		t.Skip("missing testdata/enwik8")
	}

	r := bytes.NewReader(data)
	m := map[rune]uint32{}
	for {
		c, _, err := r.ReadRune()
		if err != nil {
			if err != io.EOF {
				t.Fatal(err)
			}

			break
		}

		if c == 0 {
			panic(todo(""))
		}

		m[c]++
	}
	var sf0 []SymbolFrequency
	for k, v := range m {
		sf0 = append(sf0, SymbolFrequency{uint32(k), uint32(v)})
	}
	t.Run("bitz", func(t *testing.T) {
		sf := append([]SymbolFrequency(nil), sf0...)
		c, err := NewCodec(sf)
		if err != nil {
			t.Fatal(err)
		}

		var bitCount uint32
		for _, v := range sf0 {
			len, _ := c.Encoding(v.Symbol)
			bitCount += v.Frequency * uint32(len)
		}
		t.Logf("bitCount=%s(%s bytes)", h(bitCount), h((bitCount+7)/8))

	})
	t.Run("huffman", func(t *testing.T) {
		bitCount := huffmanBits(m)
		t.Logf("bitCount=%s(%s bytes)", h(bitCount), h((bitCount+7)/8))
	})
}

func huffmanBits(m map[rune]uint32) (r uint32) {
	var nodes []*huffman.Node
	for k, v := range m {
		nodes = append(nodes, &huffman.Node{Value: huffman.ValueType(k), Count: int(v)})
	}
	return huffmanBits0(m, huffman.Build(nodes))
}

func huffmanBits0(m map[rune]uint32, n *huffman.Node) (r uint32) {
	if n == nil {
		return 0
	}

	if n.Left == nil && n.Right == nil {
		_, bits := n.Code()
		r += uint32(bits) * uint32(m[rune(n.Value)])
	}
	if n.Left != nil {
		r += huffmanBits0(m, n.Left)
	}
	if n.Right != nil {
		r += huffmanBits0(m, n.Right)
	}
	return r
}

func BenchmarkPrepare(b *testing.B) {
	data, err := os.ReadFile(filepath.Join("testdata", "enwik8"))
	if err != nil {
		b.Skip("missing testdata/enwik8")
	}

	r := bytes.NewReader(data)
	m := map[rune]int{}
	for {
		c, _, err := r.ReadRune()
		if err != nil {
			if err != io.EOF {
				b.Fatal(err)
			}

			break
		}

		m[c]++
	}
	var sf0 []SymbolFrequency
	for k, v := range m {
		sf0 = append(sf0, SymbolFrequency{uint32(k), uint32(v)})
	}
	b.Run("bitz", func(b *testing.B) {
		// Decode map
		//
		//	0:jnml@e5-1650:~/src/modernc.org/bitz$ go test -v -run @ -bench .
		//	goos: linux
		//	goarch: amd64
		//	pkg: modernc.org/bitz
		//	cpu: Intel(R) Xeon(R) CPU E5-1650 v2 @ 3.50GHz
		//	BenchmarkPrepare
		//	BenchmarkPrepare/bitz
		//	BenchmarkPrepare/bitz-12         	     195	   6687979 ns/op	  482090 B/op	     376 allocs/op
		//	BenchmarkPrepare/huffman
		//	BenchmarkPrepare/huffman-12      	     100	  10843020 ns/op	  767809 B/op	   12145 allocs/op
		//	PASS
		//	ok  	modernc.org/bitz	4.289s
		//	0:jnml@e5-1650:~/src/modernc.org/bitz$

		// Decode tree
		//
		//	0:jnml@e5-1650:~/src/modernc.org/bitz$ go test -v -run @ -bench .
		//	goos: linux
		//	goarch: amd64
		//	pkg: modernc.org/bitz
		//	cpu: Intel(R) Xeon(R) CPU E5-1650 v2 @ 3.50GHz
		//	BenchmarkPrepare
		//	BenchmarkPrepare/bitz
		//	BenchmarkPrepare/bitz-12         	     144	   9338339 ns/op	  556685 B/op	   12317 allocs/op
		//	BenchmarkPrepare/huffman
		//	BenchmarkPrepare/huffman-12      	     100	  11388657 ns/op	  767720 B/op	   12145 allocs/op
		//	PASS
		//	ok  	modernc.org/bitz	4.713s
		//	0:jnml@e5-1650:~/src/modernc.org/bitz$

		b.ReportAllocs()
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			sf := append([]SymbolFrequency(nil), sf0...)
			if _, err := NewCodec(sf); err != nil {
				b.Fatal(err)
			}
		}
	})
	b.Run("huffman", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			var nodes []*huffman.Node
			for _, v := range sf0 {
				nodes = append(nodes, &huffman.Node{Value: huffman.ValueType(v.Symbol), Count: int(v.Frequency)})
			}
			huffman.Build(nodes)
		}
	})
}
